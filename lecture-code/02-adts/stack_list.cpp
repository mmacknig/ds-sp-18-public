// stack_default

#include <stack>
#include <list>

using namespace std;

const int N = 1<<28;

int main(int argc, char const *argv[]) {

    stack<int, list<int>> s;

    // fill stack
    for (size_t i = 0; i < N; i++) {
        s.push(i);
    }

    // empty stack
    while(!s.empty()){
        s.pop();
    }

    return 0;
}

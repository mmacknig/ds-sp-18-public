// stack_default

#include <stack>
#include <cassert>

using namespace std;

const int N = 1<<28;

int main(int argc, char const *argv[]) {

    stack<int> s;

    // fill stack
    for (size_t i = 0; i < N; i++) {
        s.push(i);
    }

    // empty stack
    int i = N - 1;
    while(!s.empty()){
        assert(s.top() == i);
        s.pop();
        i--;
    }

    return 0;
}

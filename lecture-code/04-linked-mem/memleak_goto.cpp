#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

const int NITEMS = 1<<10;
const int TRIALS = 100;

bool duplicates(int n){
    int *randoms = new int[n];
    bool result = true;

    // creating an array with random input
    for (size_t i = 0; i < NITEMS; i++) {
        randoms[i] = rand() % 1000;
    }

    for (size_t i = 0; i < n; i++) {
        // find(begin, end, what_to_find) returns end of array if not_found
        if (find(randoms + i + 1, randoms + NITEMS, randoms[i]) != (randoms + NITEMS)){
            result = true; // duplicates found
            goto failure; // never use goto statements! they are dangerous!
        }
    }

failure:
    //delete [] randoms; // uncomment this to fix leak
    return result;
}

int main(int argc, char const *argv[]) {
    srand(time(NULL)); // seeding the rand

    for (size_t i = 0; i < TRIALS; i++) {
        if (duplicates(NITEMS)) {
            cout << "duplicates detected! " << i << endl;
        } else {
            cout << "no dupes. " << i << endl;
        }
    }
    return 0;
}

// run valgrind with options:
//valgrind --leak-check=full --show-leak-kinds=all ./a.out
